<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 *
 */
class TestController extends Controller
{
    /**
     * @return int
     */
    public function actionIndex()
    {
        $url = 'http://sin.test.local/rest/routes';
        $filename = Yii::$app->runtimePath . '/routes.xml';
        $fp = @fopen($filename, 'r');

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_PUT => true,
            CURLOPT_INFILE => $fp,
            CURLOPT_INFILESIZE => filesize($filename),
            CURLOPT_TIMEOUT => 40,
            CURLOPT_ENCODING => 'gzip',
            CURLOPT_SSL_VERIFYPEER => false
        ));
        $result = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        var_dump([
            'code' => $code,
            'result' => @json_decode($result, true),
        ]);

        return ExitCode::OK;
    }
}
