<p align="center">
    <h1 align="center">sintegrum-test</h1>
    <br>
</p>

Тестовое задание: https://github.com/realbattletoad/php-test-task

1. Рест контроллер controllers/RestController.php читает переданный файл xml и возвращает ответ в формате джсон. (в задании не сказано о формате ответа поэтому джсон)
2. commands/TestController.php - использовал для теста.

array(2) {
["code"]=>
int(200)
["result"]=>
array(3) {
["route"]=>
array(6) {
[0]=>
string(10) "KIEV/PARIS"
[1]=>
string(12) "PARIS/MADRID"
[2]=>
string(20) "MADRID/PALMA DE MALL"
[3]=>
string(20) "PALMA DE MALL/MADRID"
[4]=>
string(12) "MADRID/PARIS"
[5]=>
string(10) "PARIS/KIEV"
}
["break_points"]=>
array(1) {
[0]=>
string(0) ""
}
["end_point"]=>
string(13) "PALMA DE MALL"
}
}

такой результат для файла который в тесте. т.е. конечная точка PALMA DE MALL
точек разрыва не было
