<?php
/**
 * Created by: Oleg Varenko
 * Date: 14.12.2021
 * Time: 16:54
 */

namespace app\controllers;

use app\models\test\ParseRoutesModel;
use app\models\test\UploadModel;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class RestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionRoutes() {

        $upload = new UploadModel();
        if(is_null($upload->routesXml)) {
            return ['errors' => $upload->uploadErrors];
        }

        $model = new ParseRoutesModel();
        $model->parseRoutes($upload->routesXml);

        return $model->getResponse();
    }
}