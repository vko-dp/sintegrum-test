<?php
/**
 * Created by: Oleg Varenko
 * Date: 14.12.2021
 * Time: 16:10
 */

namespace app\models\test;

use Yii;
use yii\base\Model;

/**
 * @property array $uploadErrors
 * @property RoutesXml $routesXml
 */
class UploadModel extends Model
{
    /** @var RoutesXml */
    protected $_routesXml = false;
    /** @var array  */
    protected $_uploadErrors = [];

    /**
     * @return RoutesXml|null
     */
    public function getRoutesXml() {

        if($this->_routesXml === false) {

            $this->_routesXml = null;
            $data = @file_get_contents('php://input');

            if(!empty($data)) {

                $xml = simplexml_load_string($data, RoutesXml::class);
                if($xml === false) {

                    $errors = libxml_get_errors();
                    $xmlArr = explode("\n", $data);

                    foreach ($errors as $error) {
                        $this->_uploadErrors[] = $this->_getXmlError($error, $xmlArr);
                    }

                    libxml_clear_errors();
                } else {

                    $this->_routesXml = $xml;
                }
            }
        }
        return $this->_routesXml;
    }

    /**
     * @return array
     */
    public function getUploadErrors() {
        return $this->_uploadErrors;
    }

    /**
     * @param $error
     * @param $xml
     * @return string
     */
    protected function _getXmlError($error, $xml) {

        $return  = $xml[$error->line - 1] . "\n";
        $return .= str_repeat('-', $error->column) . "^\n";

        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "Warning $error->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "Error $error->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "Fatal Error $error->code: ";
                break;
        }

        $return .= trim($error->message) .
            "\n  Line: $error->line" .
            "\n  Column: $error->column";

        if ($error->file) {
            $return .= "\n  File: $error->file";
        }

        return "{$return}\n\n--------------------------------------------\n\n";
    }
}