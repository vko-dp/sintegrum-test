<?php
/**
 * Created by: Oleg Varenko
 * Date: 14.12.2021
 * Time: 16:49
 */

namespace app\models\test;

use Yii;
use yii\base\Model;

class ParseRoutesModel extends Model
{
    /** @var RoutesXml[]  */
    protected $_roundTrip = [];

    /**
     * @param RoutesXml $routesXml
     */
    public function parseRoutes(RoutesXml $routesXml) {

        /** @var RoutesXml[] $airSegmentsFrom */
        $airSegmentsFrom = [];
        foreach($routesXml->AirSegments->AirSegment as $item) {
            $airSegmentsFrom[] = $item;
        }

        usort($airSegmentsFrom, function(RoutesXml $a, RoutesXml $b) {
            $timeA = strtotime("{$a->Departure['Date']} {$a->Departure['Time']}");
            $timeB = strtotime("{$b->Departure['Date']} {$b->Departure['Time']}");
            if ($timeA == $b) {
                return 0;
            }
            return ($timeA < $timeB) ? -1 : 1;
        });

        foreach($airSegmentsFrom as $key => $item) {
            $this->_roundTrip[] = $item;
            if($key != 0) {
                if($airSegmentsFrom[0]->equalsCityBoard($item)) {
                    break;
                }
                $item->calculateParams($airSegmentsFrom[$key - 1]);
            }
        }
    }

    /**
     * @return array
     */
    public function getResponse() {

        $response = [
            'route' => [],
            'break_points' => [],
            'end_point' => null,
        ];

        $endPoint = null;
        $citiesResidenceTime = [];
        foreach($this->_roundTrip as $item) {

            $cityItem = $item->getCityName('Board');
            if(!isset($citiesResidenceTime[$cityItem])) {
                $citiesResidenceTime[$cityItem] = 0;
            }
            $citiesResidenceTime[$cityItem] += $item->residenceTime;
            $response['route'][] = $cityItem . '/' . $item->getCityName('Off');
            if($item->breakPoint instanceof  RoutesXml) {
                $city = $item->breakPoint->getCityName('Off');
                if(!in_array($city, $response['break_points'])) {
                    $response['break_points'][] = $city;
                }
            }
        }

        if(!empty($citiesResidenceTime)) {
            $citiesResidenceTime = array_flip($citiesResidenceTime);
            $maxTime = max(array_keys($citiesResidenceTime));
            $response['end_point'] = $citiesResidenceTime[$maxTime];
        }

        return $response;
    }
}