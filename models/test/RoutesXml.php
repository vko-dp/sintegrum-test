<?php
/**
 * Created by: Oleg Varenko
 * Date: 14.12.2021
 * Time: 16:29
 */

namespace app\models\test;

class RoutesXml extends \SimpleXMLElement
{
    /** @var int */
    public $residenceTime = 0;
    /** @var RoutesXml|null  */
    public $breakPoint;

    /**
     * @param RoutesXml $item
     * @return bool
     */
    public function equalsCityBoard(RoutesXml $item) {
        $cityName = $this->getCityName('Board');
        $itemCityName = $item->getCityName('Off');
        return isset($cityName, $itemCityName) && $itemCityName == $cityName;
    }

    /**
     * @param $property
     * @return string|null
     */
    public function getCityName($property) {
        $names = explode('/', (string)$this->$property['City']);
        return isset($names[0]) ? trim($names[0]) : null;
    }

    /**
     * @param RoutesXml $item
     */
    public function calculateParams(RoutesXml $item) {
        $this->residenceTime = strtotime("{$this->Departure['Date']} {$this->Departure['Time']}") - strtotime("{$item->Arrival['Date']} {$item->Arrival['Time']}");
        $this->breakPoint = !$this->equalsCityBoard($item) ? $item : 0;
    }
}